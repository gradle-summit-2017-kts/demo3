


// configurations is a ConfigurationContainer which is a NamedDomainObjectContainer<Configuration>
configurations {

    "myConfiguration" { // will create if not there
        isVisible = false
    }

}



// tasks is a TaskContainer which is a (Polymorphic)NamedDomainObjectContainer<Task>
tasks {

    val hello by creating { // explicitly create element, fail if already there
        doLast { print("Hello, ") }
    }

    val world by creating {
        dependsOn(hello)
        doLast { println("world!") }
    }
}



tasks {

    val hello by getting // explicit access to existing element, fail if not there
    val world by getting
    world.dependsOn(hello)

}



tasks {

    "hello" {
        doLast { print("wonderful ") }
    }

    "world" { // will create the task if it's not there
        dependsOn("hello")
        doLast { println("These pretzels are making me thirsty!") }
    }
}
